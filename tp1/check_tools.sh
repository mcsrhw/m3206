#!/bin/bash

# lole copirigue mcsrhw

#Liste contenant les packet a verifier
lol=(git tmux vim htop)

for pkg in ${lol[@]}
do
#dpkg -s verifie l'etat dun package et &> redirige le stdout et le stderr 
	dpkg -s $pkg &> /tmp/lol.txt
	if [ $? -eq 0 ]
	then
		echo "[...] $pkg : installé       [...]"
	else
		echo "[/!\] $pkg: pas installé   [/!\]"
	fi
done
#Supprime le fichier utilisé
rm /tmp/lol.txt
exit 0
