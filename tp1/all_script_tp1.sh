#!/bin/bash

#lole copirigue mcsrhw

#-------------------------------------------------
#-------- CHECK INTERNET -------------------------
#-------------------------------------------------

echo "[...] Checking internet lole [...]"

# La ligne suivante essaie de t�lecharger la page d'accueil de google.fr
wget -q google.fr

# La variable $? renvoi un code concernant l'etat de la derniere commande tapé, si il est = a 0 alors elle s'est bien deroulé
if [ $? -ne 0 ]
#Si $? est different de 0
then
	echo "[!] Not connected to Internet      [!]"
	echo "[!] Please check configuration	 [!]"
	exit 1
#Si $? = 0
else
	rm index.html
	echo "[...] Internet access ok      [...]"

fi

#-------------------------------------------------                           │
#-------- SYS UPDATE --- -------------------------                           │for pkg in ${lol[@]}
#------------------------------------------------- 

#Verif des droits
# La ligne qui suit verifier si lutilisateur actuel est root
if [ "$USER" = "root" ]
then
	echo "[...] Droits ok [...]"
else
	echo "[!] Vous devez etre super-utilisateur [!]"
	exit 1
fi

if [ $? -eq 0 ]
then
	echo "Connection ok"
else
	echo "Probleme de connection"
	exit 2
fi

#Update la database
echo "[...] update database [...]"
# le --yes permet de dire yes a chaque fois ou on demande un truc, --force-yes permet de le faire dans certains autres cas
apt-get update --yes --force-yes
if [ $? -ne 0 ]
then
	echo "Erreur pendant lupdate"
	exit 3
fi

#Upgrader le systeme
echo "[...] upgrade system  [...]"
apt-get upgrade --yes --force-yes
if [ $? -ne 0 ]
then
	echo "Erreur pendant lupgrade"
	exit 4
fi

echo "Mise a jour terminee"

#-------------------------------------------------                           │
#-------- CHECK TOOLS -- -------------------------                           │for pkg in ${lol[@]}
#------------------------------------------------- 


#Liste contenant les packet a verifier
lol=(git tmux vim htop)

for pkg in ${lol[@]}
do
#dpkg -s verifie l'etat dun package et &> redirige le stdout et le stderr 
	dpkg -s $pkg &> /tmp/lol.txt
	if [ $? -eq 0 ]
	then
		echo "[...] $pkg : installé       [...]"
	else
		echo "[/!\] $pkg: pas installé   [/!\]"
	fi
done
#Supprime le fichier utilisé
rm /tmp/lol.txt


#-------------------------------------------------                           │
#-------- CHECK SSH ---- -------------------------                           │for pkg in ${lol[@]}
#------------------------------------------------- 

#Verif si intallee
dpkg -s ssh &> /tmp/lol.txt
if [ $? -ne 0 ]
then
	echo "[/!\] ssh: le service n'est pas instalee�  [/!\]"
	exit 1
else
	echo "[...] ssh: est installee é                [...]"
#verif si lance
	service ssh status &> /tmp/lol.txt
	if [ $? -ne 0 ]
	then
		echo "[/!\] ssh: le service n'est pas lanceé  [/!\]"
		exit 2
	else
		echo "[...] ssh: fonctionne     [...]"
	fi
fi
rm /tmp/lol.txt
exit 0

