#!/bin/bash

# lole copirigue mcsrhw

#Verif des droits
# La ligne qui suit verifier si lutilisateur actuel est root
if [ "$USER" = "root" ]
then
	echo "[...] Droits ok [...]"
else
	echo "[!] Vous devez etre super-utilisateur [!]"
	exit 1
fi

#Verif connectivite
./check_internet.sh
if [ $? -eq 0 ]
then
	echo "Connection ok"
else
	echo "Probleme de connection"
	exit 2
fi

#Update la database
echo "[...] update database [...]"
# le --yes permet de dire yes a chaque fois ou on demande un truc, --force-yes permet de le faire dans certains autres cas
apt-get update --yes --force-yes
if [ $? -ne 0 ]
then
	echo "Erreur pendant lupdate"
	exit 3
fi

#Upgrader le systeme
echo "[...] upgrade system  [...]"
apt-get upgrade --yes --force-yes
if [ $? -ne 0 ]
then
	echo "Erreur pendant lupgrade"
	exit 4
fi

echo "Mise a jour terminee"
exit 0

