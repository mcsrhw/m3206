#!/bin/bash

# lole copirigue mcsrhw

#nom sous forme AAAA_MM_JJ_hhmm_repertoire.tar.gz
name=`date +%Y_%m_%d_%H%M_$(basename $1).tar.gz`

echo "creation de l'archive: $name"
#creation du dossier et cd dedans 
mkdir -p /tmp/backup
cd /tmp/backup
#creation fichier chemin absolu
touch "$1abs"
cat "$1abs" < $1
#creation de l'archive
tar -zcf $name $1 &> /tmp/lole.txt
rm /tmp/lole.txt

