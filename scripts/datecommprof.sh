#!/bin/bash

#lole

if [ -z "$3" ]
then
	echo "The command takes 3 arguments."
        echo "usage: $0 [first|last] command file"
	exit 3
fi


if [ ! -f $3 ]
then
	echo "Le fichier $3 n\'existe pas."
	echo "usage: $0 [first|last] command file"
	exit 2
fi

test=$(cat $3 | grep $2 | cut -d " " -f 2 | cut -d ":" -f 1 | sort -n | head -n 1)
if [ -z "$test" ]
then
	echo "The history does not contain $2 command"
	exit 3
fi

if [ $1 = "first" ]
then
	ts=$(cat $3 | grep $2 | cut -d " " -f 2 | cut -d ":" -f 1 | sort -n | head -n 1)
	date -d@$ts
elif [ $1 = "last" ]
then
	ts=$(cat $3 | grep $2 | cut -d " " -f 2 | cut -d ":" -f 1 | sort -nr | head -n 1)
	date -d@$ts 
else
	echo "first argument should be first or last"
	echo "usage: $0 [first|last] command file"
	exit 1
fi
exit 0
